export interface SetPasswordDTO {
    password: string
    passwordConfirmation: string
}