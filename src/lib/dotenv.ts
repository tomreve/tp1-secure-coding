export const getEnvs = () => {
    const port = process.env.DATABASE_PORT;
    const host = process.env.DATABASE_HOST;
    const username = process.env.DATABASE_USERNAME;
    const password = process.env.DATABASE_PASSWORD;
    const databaseName = process.env.DATABASE_DATABASE;
    const fastifyPort = process.env.FASTIFY_PORT;
    const fastifyAddress = process.env.FASTIFY_ADDR;
    const fastifyLogging = process.env.FASTIFY_LOGGING === 'true';
    if (!port || !host || !username || !password || !databaseName || !fastifyPort || !fastifyAddress) {
        throw new Error('Missing environment variables');
    }
    return { port, host, username, password, databaseName, fastifyPort, fastifyAddress, fastifyLogging };
}