import { DataSource } from 'typeorm'
import * as dotenv from 'dotenv'
import { User } from '../entities/User';
import { UserSubscriber } from '../subscribers/user-subscriber';
import { getEnvs } from './dotenv';
dotenv.config()

export const AppDataSource = ((): DataSource =>{
    const { port, host, username, password, databaseName } = getEnvs();

    return new DataSource({
        type: "postgres",
        host,
        port: parseInt(port),
        username,
        password,
        database: databaseName,
        synchronize: true,
        logging: true,
        entities: [User],
        subscribers: [UserSubscriber],
        migrations: [],
    })
})()
