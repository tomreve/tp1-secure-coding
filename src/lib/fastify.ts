import Fastify, { FastifyInstance, RouteOptions } from 'fastify'
import { webApiRoutes } from '../routes/web-api/web-api-routes'
import { getEnvs } from './dotenv'

const { fastifyLogging } = getEnvs()

export const server: FastifyInstance = Fastify({
  logger: fastifyLogging,
  ajv: {
    customOptions: {
      removeAdditional: false
    }
  }
})
.addHook('onRoute', assertsResponseSchemaPresenceHook)
.addHook('onRoute', assertsBodySchemaPresenceHook)
.addHook('onRoute', assertsParamsSchemaPresenceHook)
.addHook('onRoute', assertsQuerySchemaPresenceHook)
.register(webApiRoutes, {prefix: '/web-api'})

export function assertsResponseSchemaPresenceHook (routeOptions: RouteOptions) {
  if(routeOptions.schema?.response === undefined){
    throw new Error('Missing response schema');
  }
}

export function assertsBodySchemaPresenceHook (routeOptions: RouteOptions) {
  if(routeOptions.schema?.body === undefined){
    throw new Error('Missing body schema');
  }
}

export function assertsParamsSchemaPresenceHook (routeOptions: RouteOptions) {
  if(routeOptions.schema?.params === undefined){
    throw new Error('Missing params schema');
  }
}

export function assertsQuerySchemaPresenceHook (routeOptions: RouteOptions) {
  if(routeOptions.schema?.querystring === undefined){
    throw new Error('Missing query schema');
  }
}