import {
    registerDecorator,
    ValidationOptions,
    ValidatorConstraint,
    ValidatorConstraintInterface,
} from 'class-validator';

import { AppDataSource } from '../lib/data-source';
import { User } from '../entities/User';

@ValidatorConstraint({ async: true })
export class UniqueInColumnConstraint implements ValidatorConstraintInterface {
    async validate(email: string) {
        const result = await AppDataSource.getRepository(User).findOne({ where: { email: email } }).then(user => {
            if (user) return false;
            return true;
        });
        return result;
    }

    defaultMessage(): string {
        return 'Email should be unique'
    }
}

export function UniqueInColumn(validationOptions?: ValidationOptions) {
    return function (object: User, propertyName: string) {
        registerDecorator({
            name: 'UniqueInColumn',
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            constraints: [],
            validator: UniqueInColumnConstraint,
        });
    };
}