import { Entity, PrimaryGeneratedColumn, Column, Index } from "typeorm"
import { IsNotEmpty, ValidationError } from "class-validator"
import * as bcrypt from "bcrypt"
import { UniqueInColumn } from "../decorators/unique-in-column-decorator"
import { SetPasswordDTO } from "../dto/password-dto"
import { validatePassword } from "../helpers/password-helper"


@Entity()
@Index("USER_EMAIL_INDEX", { synchronize: false })
export class User {
    @PrimaryGeneratedColumn("uuid")
    id!: string;
  
    @Column()
    @IsNotEmpty()
    firstname!: string

    @Column()
    @IsNotEmpty()
    lastname!: string

    @Column(
    { 
        transformer: {
          from: (value: string) => value,
          to: (value: string) => value.toLowerCase()
        }
    })
    @IsNotEmpty()
    @UniqueInColumn()
    email!: string

    @Column()
    @IsNotEmpty()
    passwordHash!: string

    async setPassword(params: SetPasswordDTO) {
      const { password, passwordConfirmation } = params
      if (password !== passwordConfirmation){
        const differentPasswordError = new ValidationError()
        differentPasswordError.target = this
        differentPasswordError.property = 'passwordHash'
        differentPasswordError.constraints = { arePasswordDifferent: 'password and passwordConfirmation should be the same' }
        throw differentPasswordError
      } else {
        if(!validatePassword(password)){
          const notValidPasswordError = new ValidationError()
          notValidPasswordError.target = this
          notValidPasswordError.property = 'passwordHash'
          notValidPasswordError.constraints = { isValidPassword : 'password should have an entropy >= 80' }
          throw notValidPasswordError
        }
        this.passwordHash = await bcrypt.hash(password, 10)
      }
  }

  async isPasswordValid(password: string){
    return bcrypt.compare(password, this.passwordHash)
  }
}