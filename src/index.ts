import "reflect-metadata"
import { AppDataSource } from './lib/data-source'
import { getEnvs } from './lib/dotenv'
import { server } from './lib/fastify'


async function run(){
    await AppDataSource.initialize()
    const { fastifyAddress, fastifyPort } = getEnvs();
    await server.listen({ port: parseInt(fastifyPort), host: fastifyAddress })
}

void run()