export const CreateSessionRequestBodySchema = {
    type: 'object',
    properties: {
        email: {
            type: 'string'
        },
        password: {
            type: 'string'
        }
    },
    additionalProperties: false,
    required: ['email','password']
} as const;