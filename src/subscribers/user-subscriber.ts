import { validate } from "class-validator";
import { EntitySubscriberInterface, EventSubscriber, InsertEvent, UpdateEvent } from "typeorm"
import { User } from "../entities/User"

@EventSubscriber()
export class UserSubscriber implements EntitySubscriberInterface<User> {
    listenTo() {
        return User
    }

    async beforeInsert(event: InsertEvent<User>) {
        await this.validate(event.entity);
    }

    async beforeUpdate(event: UpdateEvent<User>) {
        await this.validate(event.entity as User);
    }

    private async validate(user: User) {
        const errors = await validate(user);
        if (errors.length > 0) {
            throw errors[0];
        }
    }
    
}