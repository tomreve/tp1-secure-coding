import { FastifyInstance } from "fastify";
import { FromSchema } from "json-schema-to-ts";
import { CreateUserRequestBodySchema } from "../../schemas/create-user-request-body-schema"
import { AppDataSource } from '../../lib/data-source';
import { User } from '../../entities/User'
import { CreateUserResponseBodySchema } from '../../schemas/create-user-response-body-schema';
import { EmptyParamsSchema } from "../../schemas/empty-params-schema";
import { EmptyQuerySchema } from "../../schemas/empty-query-schema";

export async function webApiRoutes(fastify: FastifyInstance){
    fastify.post<{ Body: FromSchema<typeof CreateUserRequestBodySchema>, Reply: FromSchema<typeof CreateUserResponseBodySchema> }>(
        '/users',
        {
          schema: {
            params: EmptyParamsSchema,
            querystring: EmptyQuerySchema,
            body: CreateUserRequestBodySchema,
            response: {
              201: CreateUserResponseBodySchema
            },
          }
        },
        async (request, reply): Promise<void> => {
          const userRepository = AppDataSource.getRepository(User);
          const {firstname, lastname, email, password, passwordConfirmation} = request.body;
          const user = userRepository.create({
            email: email,
            firstname: firstname,
            lastname: lastname,
          });
          await user.setPassword({password: password, passwordConfirmation: passwordConfirmation});
          await userRepository.save(user);
          await reply.status(201).send(user);
        },
      );
}