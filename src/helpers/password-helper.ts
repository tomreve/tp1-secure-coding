/** Based on https://github.com/autonomoussoftware/fast-password-entropy/blob/master/src/index.js */

export function computePasswordEntropy(password: string): number {
    const stdCharsets = [
        {
            name: 'lowercase',
            re: /[a-z]/,
            length: 26
        }, 
        {
            name: 'uppercase',
            re: /[A-Z]/,
            length: 26
        },
        {
            name: 'numbers',
            re: /[0-9]/,
            length: 10
        },
        {
            name: 'symbols',
            re: /[^a-zA-Z0-9]/,
            length: 33
        }
    ];
    
    const passwordCharsetLength = stdCharsets.reduce(
        (length, charset) => length + (charset.re.test(password) ? charset.length : 0),
        0
    );

    const passwordLength = password.length;

    const passwordEntropy = Math.round(passwordLength * Math.log(passwordCharsetLength) / Math.LN2);

    return passwordEntropy;
}

export function validatePassword(password: string): boolean {
    return computePasswordEntropy(password) >= 80;
}