import Fastify, { FastifyInstance } from 'fastify'
import { assertsBodySchemaPresenceHook, assertsParamsSchemaPresenceHook, assertsQuerySchemaPresenceHook, assertsResponseSchemaPresenceHook } from '../../lib/fastify'
import * as chai from 'chai'

describe('Fastify', function () {
    describe('assertsSchemaPresenceHook', function () {
        it('no response schema in route should throw an error', function () {
            const fastify: FastifyInstance = Fastify()
            .addHook('onRoute', assertsResponseSchemaPresenceHook);
            
            chai.expect(() => {
                fastify.route({
                    method: 'GET',
                    url: '/totaly-random-path',
                    handler: function (request, reply) {
                        reply.send({ hello: 'world' })
                    }
                });
            }).to.throw('Missing response schema')
        })

        it('no body schema in route should throw an error', function () {
            const fastify: FastifyInstance = Fastify()
            .addHook('onRoute', assertsBodySchemaPresenceHook);
            
            chai.expect(() => {
                fastify.route({
                    method: 'POST',
                    url: '/totaly-random-path',
                    handler: function (request, reply) {
                        reply.send({ hello: 'world' })
                    }
                });
            }).to.throw('Missing body schema')
        })

        it('no query schema in route should throw an error', function () {
            const fastify: FastifyInstance = Fastify()
            .addHook('onRoute', assertsQuerySchemaPresenceHook);
            
            chai.expect(() => {
                fastify.route({
                    method: 'GET',
                    url: '/totaly-random-path',
                    handler: function (request, reply) {
                        reply.send({ hello: 'world' })
                    }
                });
            }).to.throw('Missing query schema')
        })

        it('no params schema in route should throw an error', function () {
            const fastify: FastifyInstance = Fastify()
            .addHook('onRoute', assertsParamsSchemaPresenceHook);
            
            chai.expect(() => {
                fastify.route({
                    method: 'GET',
                    url: '/totaly-random-path',
                    handler: function (request, reply) {
                        reply.send({ hello: 'world' })
                    }
                });
            }).to.throw('Missing params schema')
        })
    })
})