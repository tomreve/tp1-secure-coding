import * as chai from 'chai'
import { computePasswordEntropy, validatePassword } from '../../helpers/password-helper'

describe('Password', function () {
    it('password with 16 characters from 0 to 9 and A to Z should have an entropy of 83', () => {
        const expectedEntropy = 83;
        const password = "A0B1C2D3E4F5G6H7";
        const passwordEntropy = computePasswordEntropy(password);

        chai.expect(passwordEntropy).to.equal(expectedEntropy)
    })

    it('password with 16 characters from 0 to 9 and A to Z should be valid', () => {
        const password = "A0B1C2D3E4F5G6H7";
        const isPasswordValid = validatePassword(password);

        chai.expect(isPasswordValid).to.equal(true)
    })
})