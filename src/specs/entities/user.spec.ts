import * as chai from 'chai'
import * as chaiAsPromised from 'chai-as-promised'
import { DataSource } from 'typeorm'
import { AppDataSource } from "../../lib/data-source"
import { SetPasswordDTO } from '../../dto/password-dto'
import { User } from '../../entities/User'

chai.use(chaiAsPromised)

export const datasource: DataSource = AppDataSource

export async function clearDB() {
    const entities = datasource.entityMetadatas;
    for (const entity of entities) {
      const repository = datasource.getRepository(entity.name);
      await repository.query(`TRUNCATE "${entity.tableName}" RESTART IDENTITY CASCADE;`);
    }
  }

describe('User', function () {
    before(async function () {
        await datasource.initialize()
    })
        
    beforeEach(async function () {
        await clearDB();
    })

    describe('validations', function () {
        it('should create a new User in database', async() => {
            const userRepository = datasource.getRepository(User);
            const user = userRepository.create({
                email: "jdoe@gmail.com",
                firstname: "John",
                lastname: "Doe",
                passwordHash: ""
            });

            const passwordDTO: SetPasswordDTO = {password:'eWA26qasUkZPgSa5', passwordConfirmation: 'eWA26qasUkZPgSa5'}

            await user.setPassword(passwordDTO)
            await userRepository.save(user)

            const userFromDatabase = await datasource.getRepository(User).findOne({where: {email: user.email}})
            chai.expect(userFromDatabase).to.not.be.null
            chai.expect(userFromDatabase?.email).to.equal(user.email)
        })

        it('should raise error if email is missing', async function () {
            const userRepository = datasource.getRepository(User);
            const user = userRepository.create({
                email: null as unknown as string,
                firstname: "John",
                lastname: "Doe",
                passwordHash: "passwordHash"
            });
            await chai.expect(userRepository.save(user)).to.eventually.be.rejected.and.deep.include({
                target: user,
                property: 'email',
                constraints: { isNotEmpty: 'email should not be empty' }
            })        
        })

        it('should set user email to lowercase', async function () {
            const userRepository = datasource.getRepository(User);
            const user = userRepository.create({
                email: "JDOE@GMAIL.COM",
                firstname: "John",
                lastname: "Doe",
                passwordHash: "passwordHash"
            });
            await userRepository.save(user)

            const userFromDatabase = await userRepository.findOne({ where: { email: user.email}})
            chai.expect(userFromDatabase).to.haveOwnProperty('email').and.be.equal('jdoe@gmail.com')
        })

        it('should raise error if email already exsist', async function () {
            const userRepository = datasource.getRepository(User);
            const user = userRepository.create({
                email: "JDOE@GMAIL.COM",
                firstname: "John",
                lastname: "Doe",
                passwordHash: "passwordHash"
            });
            await userRepository.save(user)
            
            const userWithSameEmail = userRepository.create({
                email: "JDOE@GMAIL.COM",
                firstname: "Fred",
                lastname: "Paf",
                passwordHash: "passwordHash"
            });

            await chai.expect(userRepository.save(userWithSameEmail)).to.eventually.be.rejected.and.deep.include({
                target: userWithSameEmail,
                property: 'email',
                constraints: { UniqueInColumnConstraint: 'Email should be unique' }
            })
        })

        it('different password and password confirmation should raise an error', async function () {
            const userRepository = datasource.getRepository(User);
            const user = userRepository.create({
                email: "JDOE@GMAIL.COM",
                firstname: "John",
                lastname: "Doe",
                passwordHash: ""
            });

            const passwordDTO: SetPasswordDTO = {password:'eWA26qasUkZPgSa5', passwordConfirmation: 'eWA26qasUkZPgSa9'}

            await chai.expect(user.setPassword(passwordDTO)).to.eventually.be.rejected.and.deep.include({
                target: user,
                property: 'passwordHash',
                constraints: { arePasswordDifferent: 'password and passwordConfirmation should be the same' }
            })
        })

        it('weak password should raise a validation error', async function () {
            const userRepository = datasource.getRepository(User);
            const user = userRepository.create({
                email: "JDOE@GMAIL.COM",
                firstname: "John",
                lastname: "Doe",
                passwordHash: ""
            });

            const passwordDTO: SetPasswordDTO = {password:'password', passwordConfirmation: 'password'}

            await chai.expect(user.setPassword(passwordDTO)).to.eventually.be.rejected.and.deep.include({
                target: user,
                property: 'passwordHash',
                constraints: { isValidPassword : 'password should have an entropy >= 80' }
            })
        })

        it('hashed password should match initial password', async() => {
            const userRepository = datasource.getRepository(User);
            const user = userRepository.create({
                email: "jdoe@gmail.com",
                firstname: "John",
                lastname: "Doe",
                passwordHash: ""
            });

            const passwordDTO: SetPasswordDTO = {password:'eWA26qasUkZPgSa5', passwordConfirmation: 'eWA26qasUkZPgSa5'}

            await user.setPassword(passwordDTO)
            await userRepository.save(user)

            const userFromDatabase = await datasource.getRepository(User).findOne({where: {email: user.email}})
            chai.expect(await userFromDatabase?.isPasswordValid("eWA26qasUkZPgSa5")).to.equal(true)
        })
    })
})