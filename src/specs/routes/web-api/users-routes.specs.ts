import { server } from '../../../lib/fastify'
import { User } from '../../../entities/User'
import { CreateUserRequestBodySchema } from '../../../schemas/create-user-request-body-schema'
import { FromSchema } from 'json-schema-to-ts'
import { datasource } from '../../entities/user.spec'
import * as chai from 'chai'

describe('/web-api/users', function () {
    describe('POST #create', function () {
        it('should register the user', async function () {
            const payload: FromSchema<typeof CreateUserRequestBodySchema> = {
                firstname: "John",
                lastname: "Doe",
                email: "john.doe@domain.com",
                password: "eWA26qasUkZPgSa5",
                passwordConfirmation: "eWA26qasUkZPgSa5"
            }
            const response = await server.inject({ url: `/web-api/users`, method: 'POST', payload: payload })
            chai.expect(response.statusCode).to.equal(201);
            chai.expect(response.json()).to.have.keys('id', 'firstname', 'lastname', 'email').and.to.not.have.keys('passwordHash');
            const userFromDatabase = await datasource.getRepository(User).findOne({where: {email: payload.email}})
            chai.expect(userFromDatabase).to.not.be.null
            chai.expect(userFromDatabase?.email).to.equal(payload.email)
        })
    })

    it('additional properties in payload should throw a 400', async function () {
        const payload = {
            firstname: "John",
            lastname: "Doe",
            email: "john.doe@domain.com",
            password: "eWA26qasUkZPgSa5",
            passwordConfirmation: "eWA26qasUkZPgSa5",
            addiotionalProperty: "a property that as nothing to do here"
        }
        const response = await server.inject({ url: `/web-api/users`, method: 'POST', payload: payload })
        chai.expect(response.statusCode).to.equal(400);
    })
})