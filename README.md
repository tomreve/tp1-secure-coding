# tp1-secure-coding

## Question 1 : Why should you reset the database before each test case? Give examples of issues you may meet otherwise.
By reseting the database, your tests run with a clean state. Entries in the database could make the result of our tests less predictable. Previoulsy added entries may cause side effects between each test, that's why you should reset the database before each test case.

Issues examples : 

-  For example let's suppose that a test that we run earlier put in the databases rows that should not be in it like rows with missing data. The test should have not let these data in the databases. These data could raise error in subsequent test that should have been true otherwise.
-  Each test have their own setup, they should be running in any order and have to be independent from eachothers.

## Question 2 : What kind of error is currently thrown in test case "should raise error if email is missing"? Is it an SQL error (occurring in the database server) or a validation error before the query got executed? What should it be, so it is easy and secure to format an error message to the end user (considering security, message internationalisation, etc.)?

Actually, in the test case "should raise error if email is missing", an SQL error is thrown. It would be better if a validation error was thrown to make sure that the object is well-formed before trying to do the SQL request. With this, the error returned could be human-readable and would not leak any technical information about the app.

## Question 3 - Why do we need both a database constraint and a validation in typescript for the same check?

Typescript validation will prevent the application to try SQL queries if the data are not valid and allow us to get more readable error messages, that's why we need both a database constraint and a validation in typescript for the same check.

## Question 4: how models validations, such as the one you just wrote, can serve the security of your application? Give an example. In addition, which database mechanism can be leveraged for security hardening in case a validation fails (ex. while persisting 2 entities in response to the same action)? Clue: the mechanism I am thinking about could also operate on afterUpdate subscriptions.

By examining the format of the email (or any other field) the user has submitted before the query is done, model validations can contribute to the security of our application. In our case, the email is a really important field because it allow the user to login, so we really don't want two users with the same email. Also, using models validation can prevent sql injection by striping undesired characters typed by the user. Finaly, the model validation is done before sql call, so it can avoid unusefull database request and reduce energy consumption.

In case the validation fails (eg. persisiting 2 entities at the same time), we also can do another check on the afterUpdate hook and use a sql rollback transaction.

# tp2-secure-coding

## Question 1: please write a small paper about that naming convention.
The common Rest naming convention is to use nouns to name URI. We dont use for example mysite/GetUser but mysite/user as it must be manage within the HTTP request. 

Name must be clear and intuitive, there must be no abridged name.

The character '/' is used to navigate through the hierarchy of our api.
There must be no / at the end to be clearer.

Words are separated with hyphen like in this example: mysite/first-name

URI must be in lowercase. URI with capital letter might cause errors.

You should avoid special character as much as possible.

You should avoid using file extensions in URI. 

## Question 2: considering they use REST naming convention, what would do POST /web-api/users and POST /web-api/sessions endpoints?

POST /web-api/users va nous permettre de créer un nouvel user pour notre web-api.
POST /web-api/sessions va nous permettre de créer une nouvelle session pour notre web-api.

# Question 4: how behaves fastify:

•if no json schema is provided for any of body, query and params ?
It is silent dismissed by fastify.
Body cannot be empty when content-type is set to application/json.

•if the client submits an unknown property, according to the JSON schema?
It is silent dismissed by fastify.

•if the client omits a required property, according to the JSON schema?
It will raise an error.